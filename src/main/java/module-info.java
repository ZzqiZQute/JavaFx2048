module cn.zz.JavaFx2048 {
    requires javafx.controls;
    requires javafx.fxml;

    opens cn.zz.JavaFx2048 to javafx.fxml;
    exports cn.zz.JavaFx2048;
}